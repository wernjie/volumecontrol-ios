//
//  VolumeControl.swift
//
//  Created by Lim Wern Jie.
//

import UIKit
import MediaPlayer

public class VolumeControl {
    
    private static var globalInstance: VolumeControl?
    public static func load() {
        if globalInstance == nil {
            globalInstance = VolumeControl()
        }
    }
    public static var shared: VolumeControl {
        VolumeControl.load()
        return globalInstance!
    }
    
    
    private var volumeViewSlider: UISlider?
    private var configDate: TimeInterval?
    
    private init() {
        self.configure()
    }
    
    private var delayNecessary: TimeInterval {
        //Returns delay needed to modify the volume slider value
        //This is to workaround the fact that the volume slider takes some time to be configured by the system.
        return min(1, max((configDate! + 0.5) - Date().timeIntervalSince1970, 0))
    }
    
    private func configure() {
        let volumeView: MPVolumeView = MPVolumeView(frame: CGRect.zero)
        for view: UIView in volumeView.subviews {
            if (NSStringFromClass(view.classForCoder) == "MPVolumeSlider") {
                volumeViewSlider = view as? UISlider
                configDate = Date().timeIntervalSince1970
                return
            }
        }
    }
   

 
    public var available : Bool {
        return volumeViewSlider != nil && configDate != nil
    }
    
    public func setVolume(_ volume: Float) -> Bool {
        if let s = volumeViewSlider {
            DispatchQueue.main.asyncAfter(deadline: .now() + delayNecessary) {
                s.setValue(volume, animated: true)
                s.sendActions(for: .touchUpInside)
            }
            return true
        } else {
            return false
        }
    }
    
    public func maximizeVolume() -> Bool {
        if let s = volumeViewSlider {
            DispatchQueue.main.asyncAfter(deadline: .now() + delayNecessary) {
                if s.value != 1.0 {
                    s.setValue(1.0, animated: true)
                    s.sendActions(for: .touchUpInside)
                }
            }
            return true
        } else {
            return false
        }
    }
    
    public func minimizeVolume() -> Bool {
        return muteVolume()
    }

    public func muteVolume() -> Bool {
        if let s = volumeViewSlider {
            DispatchQueue.main.asyncAfter(deadline: .now() + delayNecessary) {
                if s.value != 0.0 {
                    s.setValue(0.0, animated: true)
                    s.sendActions(for: .touchUpInside)
                }
            }
            return true
        } else {
            return false
        }
    }
}

